export class Location {
    file: string
    line: number
    column: number
    constructor(file: string, line: number = 1, column: number = 1)  {
        this.line = line
        this.column = column
        this.file = file
    }
    static clone(loc: Location): Location{
        return new this(loc.file, loc.line, loc.column)
    }
    toString(): string {
        return `${this.file}:${this.line}:${this.column}`
    }
}