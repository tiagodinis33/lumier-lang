
import { readFileSync } from "fs";
import { Location } from "./location"

type File = string;
interface Token {
    token: string,
    location: Location
}
type Tokens = Token[]
class TokenizedCode {
    tokens: Tokens = []
    queue(token: Token) {
        this.tokens.push(token)
    }
    /**
     * Removes the first element of the queue and returns it
     * @returns the first element in the queue or undefined if the queue is empty
     */
    pollToken(): Token | undefined {
        return this.tokens.shift()
    }
}
function tokenize(file: File): TokenizedCode {
    let parsedCode = new TokenizedCode;
    let data = readFileSync(file, 'utf-8');
    let currentToken = "";
    let loc: Location = new Location(file)
    function calculateFirstCharOfToken(){
        return new Location(loc.file, loc.line, loc.column-currentToken.length)
    }
    function saveToken(){
        if(currentToken.length == 0) return;
        parsedCode.queue({token: currentToken, location: calculateFirstCharOfToken()});
        currentToken = "";
    }
    for (let i = 0; i < data.length; i++) {
        const char = data.at(i) as string;
        
        switch(char) {
            case "\n":
                saveToken()
                parsedCode.queue({token: char, location: calculateFirstCharOfToken()});
                loc.line++
                loc.column = 0;
                break;
            case "(":
            case ",":
            case ")":
                saveToken()
                parsedCode.queue({token: char, location: calculateFirstCharOfToken()});
                break;
            case "\t":
                console.warn("%s : Use spaces instead of tabs", calculateFirstCharOfToken())
            case " ":
                saveToken()
                break;
            default:
                currentToken += char
                break;

        }
        loc.column++;
    }
    //save token at the end of file, which is not reached by the for loop
    if(currentToken){
        saveToken()
    }
    return parsedCode
}
export {
    Location,
    TokenizedCode,
    Token,
    tokenize
};