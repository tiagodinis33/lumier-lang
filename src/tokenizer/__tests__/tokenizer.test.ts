import { Location, tokenize, TokenizedCode } from "..";

test("tokenizes correctly", () => {
    const tokens = tokenize("./test-programs/tokenizer-test");
    const expectedTokens = new TokenizedCode();
    expectedTokens.tokens = [
        {
            location: new Location("./test-programs/tokenizer-test"),
            token: "this"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 6),
            token: "text"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 11),
            token: "should"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 18),
            token: "be"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 21),
            token: "tokenized"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 31),
            token: "correctly!"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 1, 41),
            token: "\n"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 1),
            token: "("
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 2),
            token: ")"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 4),
            token: "{}"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 7),
            token: "á"
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 8),
            token: ","
        },
        {
            location: new Location("./test-programs/tokenizer-test", 2, 10),
            token: "\"\""
        },
    ]
    expect(tokens).toStrictEqual(expectedTokens)
})