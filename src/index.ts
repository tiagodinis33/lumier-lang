
import yargs from "yargs"
const args = yargs(process.argv.slice(2))
    .option(
        "simulate", {
        alias: "sim",
        type: "boolean",
        conflicts: [
            "compile"
        ],
        description: "Changes compiler to simulation/interpreted mode"
    })
    .parseSync();
const file = args._[0] as string;
if (args._.length > 1) {
    console.error("WARN: Ignoring these arguments:" + args._.slice(1))
}
import simulate from "./simulate"
import compile from "./compile"

if (args.simulate) {
    simulate(file);
} else {
    compile(file);
}